import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

class Patient
{
   static final int RETURN      = 0;
   static final int SURNAME     = 1;
   static final int FIRSTNAME   = 2;
   static final int DATEOFBIRTH = 3;

   int       id;
   String    surname;
   String    firstName;
   LocalDate dateOfBirth;
   LocalDate currentDate;
   long age;
   double length;
   double weight;
   double bmi;

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   String getSurname()
   {
      return surname;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   String getFirstName()
   {
      return firstName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Constructor
   ////////////////////////////////////////////////////////////////////////////////
   Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double length, double weight )
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      currentDate      = LocalDate.now();
      this.age         = calcAge();
      this.length      = length;
      this.weight      = weight;
      this.bmi         = calcBMI();
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Display patient data.
   ////////////////////////////////////////////////////////////////////////////////
   void viewData()
   {
      System.out.format( "===== Patient id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Surname:", surname );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Date of birth:", dateOfBirth );
      System.out.format( "%-17s %s\n", "Age:", age );
      System.out.format( "%-17s %.2f\n", "BMI:", bmi );
   }
   long calcAge(){ return ChronoUnit.YEARS.between(this.dateOfBirth, currentDate);}

   double calcBMI(){ return this.weight / (this.length * this.length);}

   ////////////////////////////////////////////////////////////////////////////////
   // Shorthand for a Patient's full name
   ////////////////////////////////////////////////////////////////////////////////
   String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, dateOfBirth.toString() );
   }
}
